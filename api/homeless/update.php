<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
// include database and object files
include_once '../config/database.php';
include_once '../objects/homeless.php';
  
// get database connection
$database = new Database();
$db = $database->getConnection();
  
// prepare product object
$homeless = new Homeless($db);
  
// get id of product to be edited
$data = json_decode(file_get_contents("php://input"));
  
// set ID property of product to be edited
$homeless->id = $data->id;

// set homeless property values
$homeless->name = $data->name;
$homeless->latitude = $data->latitude;
$homeless->longitude = $data->longitude;
$homeless->need = $data->need;
$homeless->place = $data->place;
$homeless->note =$data->note;
$homeless->type ="marker";
//$homeless->created = date('Y-m-d H:i:s');
//echo  $homeless->id;
// update homeless
if($homeless->update()){
  
    // set response code - 200 ok
    http_response_code(200);
  
    // tell the user
    echo json_encode(array("message" => "Dati aggiornati"));
}
  
// if unable to update the product, tell the user
else{
  
    // set response code - 503 service unavailable
    http_response_code(503);
  
    // tell the user
    echo json_encode(array("message" => "Impossibile aggiornare i dati."));
}
?>