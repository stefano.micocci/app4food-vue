<?php
// from https://codeofaninja.com/2017/02/create-simple-rest-api-in-php.html
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/core.php';
include_once '../shared/utilities.php';
include_once '../config/database.php';
include_once '../objects/magazine.php';

// utilities
$utilities = new Utilities();

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

// initialize object
$magazine = new Magazine($db);

// query magazines
$stmt = $magazine->readPaging($from_record_num, $records_per_page);
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){
  
    // magazines array
    $magazines_arr=array();
    $magazines_arr["records"]=array();
    $magazines_arr["paging"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
  
        $magazine_item=array(
            "id" => $id,
            "tipo_magazine" => $tipo_magazine,
            "titolo" => html_entity_decode($titolo),
            "continente" => $continente,
            "stato" => $stato,
            "regione" => $regione,
            "citta" => $citta,
            "categoria" => $categoria,
            "anno" => $anno,
            "mese" => $mese,
            "url_file" => $url_file
        );
  
        array_push($magazines_arr["records"], $magazine_item);
    }
    // include paging
    $total_rows=$magazine->count();
    $page_url="{$home_url}magazine/read_paging.php?";
    $paging=$utilities->getPaging($page, $total_rows, $records_per_page, $page_url);
    $magazines_arr["paging"]=$paging;
    
    // set response code - 200 OK
    http_response_code(200);
  
    // show magazines data in json format
    echo json_encode($magazines_arr);
}
  
// no magazines found will be here
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No magazines found.")
    );
}
?>