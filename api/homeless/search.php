<?php
// from https://codeofaninja.com/2017/02/create-simple-rest-api-in-php.html
// a file that will accept keywords parameter to search "magazines"

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/database.php';
include_once '../objects/homeless.php';
include_once '../config/core.php';
  
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

// initialize object
$magazine = new Homeless($db);

// get keywords
$keywords=isset($_GET["s"]) ? $_GET["s"] : "";

// query products
$stmt = $magazine->search($keywords);
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){
  
    // magazines array
    $magazines_arr=array();

    $magazines_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
  
        $magazine_item=array(
            "id" => $id,
            "name" => $name,
            "note" => html_entity_decode($note),
            "latitude" => $latitude,
            "longitude" => $longitude,
            "type" => $type,
            "created" => $created,
            "modified" => $modified,
            "coords" => [(float)$latitude,(float)$longitude]
        );
  
        array_push($magazines_arr["records"], $magazine_item);
    }
  
    // set response code - 200 OK
    http_response_code(200);
  
    // show magazines data in json format
    echo json_encode($magazines_arr);
}
  
// no magazines found will be here
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No homeless found.")
    );
}

?>